﻿namespace PayCompute.Entity
{
    public enum PaymentMethod
    {
        Bank,
        Check,
        Cash
    }
}