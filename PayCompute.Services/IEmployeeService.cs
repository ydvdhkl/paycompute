﻿using Microsoft.AspNetCore.Mvc.Rendering;
using PayCompute.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PayCompute.Services
{
    public interface IEmployeeService
    {
        Task CreateEmployeeAsync(Employee newEmployee);
        Employee GetEmployeeById(int employeeId);
        IEnumerable<Employee> GetAllEmployee();
        Task UpdateEmployeeAsync(Employee employee);
        Task UpdateEmployeebyIdAsync(int employeeId);
        Task DeleteEmployee(int employeeId);
        decimal UnionFees(int id);
        decimal StudentLoanRepaymentAmount(int id, decimal totalAmount);
        IEnumerable<SelectListItem> GetAllEmployeesForPayroll();
    }
}
