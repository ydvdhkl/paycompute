﻿using Microsoft.AspNetCore.Mvc.Rendering;
using PayCompute.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PayCompute.Services
{
    public interface IPayComputationService
    {
        Task CreatePaymentRecordAsync(PaymentRecord paymentRecord);
        PaymentRecord GetPaymentRecordById(int id);
        TaxYear GetTaxYearById(int id);
        IEnumerable<PaymentRecord> GetAllPaymentRecords();
        IEnumerable<SelectListItem> GetAllTaxYears();
        decimal OvertimeHours(decimal hoursWorked, decimal contractualHours);
        decimal ContractualEarnings(decimal contractualHours, decimal hoursWorked, decimal hourlyRate);
        decimal OvertimeRate(decimal hourlyRate);
        decimal OvertimeEarnings(decimal overtimeRate, decimal overtimeHours);
        decimal TotalEarnings(decimal overtimeEarnings, decimal contractualEarings);
        decimal TotalDeduction(decimal tax, decimal nic, decimal studentLoanRepayment, decimal unionFees);
        decimal NetPayment(decimal totalEarning, decimal totalDeduction);
    }
}
