﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Paycompute.Persistence;
using PayCompute.Entity;

namespace PayCompute.Services.Implementation
{
    public class EmployeeService : IEmployeeService
    {
        private decimal studentLoanAmount;

        private readonly ApplicationDbContext _applicationDbContext;
        public EmployeeService(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }
        public async Task CreateEmployeeAsync(Employee newEmployee)
        {
            await _applicationDbContext.Employees.AddAsync(newEmployee);
            await _applicationDbContext.SaveChangesAsync();
        }

        public Employee GetEmployeeById(int employeeId) => _applicationDbContext.Employees.Where(e=>e.Id == employeeId).FirstOrDefault();

        public IEnumerable<Employee> GetAllEmployee() => _applicationDbContext.Employees.AsNoTracking().OrderBy(emp => emp.FullName);

        public async Task UpdateEmployeeAsync(Employee employee)
        {
            _applicationDbContext.Update(employee);
            await _applicationDbContext.SaveChangesAsync();
        }

        public async Task UpdateEmployeebyIdAsync(int employeeId)
        {
            var employee = GetEmployeeById(employeeId);
            _applicationDbContext.Update(employee);
            await _applicationDbContext.SaveChangesAsync();
        }  

        public async Task DeleteEmployee(int employeeId)
        {
            var employee = GetEmployeeById(employeeId);
            _applicationDbContext.Remove(employee);
            await _applicationDbContext.SaveChangesAsync();
        }

        public decimal StudentLoanRepaymentAmount(int id, decimal totalAmount)
        {
            var employee = GetEmployeeById(id);

            if (employee.StudentLoan == StudentLoan.Yes && totalAmount > 1750 & totalAmount < 2000)
            {
                studentLoanAmount = 15m;
            }
            else if (employee.StudentLoan == StudentLoan.Yes && totalAmount >= 2000 & totalAmount < 2250)
            {
                studentLoanAmount = 38m;
            }
            else if (employee.StudentLoan == StudentLoan.Yes && totalAmount >= 2250 & totalAmount < 2500)
            {
                studentLoanAmount = 60m;
            }
            else if (employee.StudentLoan == StudentLoan.Yes && totalAmount >= 2500)
            {
                studentLoanAmount = 83m;
            }
            else
            {
                studentLoanAmount = 0m;
            }
            return studentLoanAmount;
        }

        public decimal UnionFees(int id)
        {
            var employee = GetEmployeeById(id);
            var fee = employee.UnionMember == UnionMember.Yes? 10m : 0m;
            return fee;
        }

        public IEnumerable<SelectListItem> GetAllEmployeesForPayroll()
        {
            return GetAllEmployee().Select(emp => new SelectListItem()
            {
                Text = emp.FullName,
                Value = emp.Id.ToString()
            });
        }
    }
}
