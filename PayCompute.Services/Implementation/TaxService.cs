﻿namespace PayCompute.Services.Implementation
{
    public class TaxService : ITaxService
    {
        private decimal taxRate;
        private decimal tax;
        public decimal TaxAmount(decimal totalAmount)
        {
            if (totalAmount <= 1024)
            {
                //tax free rate
                taxRate = 0.0m;
                //Income tax
                tax = totalAmount * taxRate;
            }
            else if (totalAmount > 1024 && totalAmount <= 3125)
            {
                //Basic tax rate
                taxRate = .20m;
                //Income tax
                tax = 1024 * .0m + ((totalAmount - 1024) * taxRate);
            }
            else if (totalAmount > 3125 && totalAmount <= 12500)
            {
                //High tax rate
                taxRate = .40m;
                //Income tax
                tax = 1024 * .0m + ((3125 - 1024) * .20m) + ((totalAmount - 3125) * taxRate);
            }
            else if (totalAmount > 12500)
            {
                //Addtional tax rate
                taxRate = .45m;
                //Income tax
                tax = tax = 1024 * .0m + ((3125 - 1024) * .20m) + ((12500 - 3125) * .40m) + ((totalAmount - 12500) * taxRate);
            }
            return tax;
        }
    }
}
