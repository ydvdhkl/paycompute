﻿
namespace PayCompute.Services.Implementation
{
    public class NationalInsuranceContributionService : INationalInsuranceContributionService
    {
        private decimal NIRate;
        private decimal NIC;
        public decimal INContribution(decimal totalAmount)
        {
            if (totalAmount < 719)
            {
                //Lower earning limit rate and below Primary Threshold
                NIRate = .0m;
                NIC = 0m;
            }
            else if (totalAmount >= 719 && totalAmount <= 4167)
            {
                //Between Primary Threshold and upper earning limit
                NIRate = .12m;
                NIC = ((totalAmount - 719) * NIRate);
            }
            else if ( totalAmount > 4167)
            {
                //Above the Upper earning limit
                NIRate = .02m;
                NIC = ((4167 - 719) * .12m) + ((totalAmount - 4167) * NIRate);
            }
            return NIC;
        }
    }
}
