﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Paycompute.Persistence;
using PayCompute.Entity;

namespace PayCompute.Services.Implementation
{
    public class PayComputationService : IPayComputationService
    {
        private decimal contractualEarnings;
        private decimal overtimeHours;

        private readonly ApplicationDbContext _applicationDbContext;
        public PayComputationService(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }
        public decimal ContractualEarnings(decimal contractualHours, decimal hoursWorked, decimal hourlyRate)
        {
            if (hoursWorked < contractualHours)
            {
                contractualEarnings = hoursWorked * hourlyRate;
            }
            else
            {
                contractualEarnings = contractualHours * hourlyRate;
            }
            return contractualEarnings;
        }

        public async Task CreatePaymentRecordAsync(PaymentRecord paymentRecord)
        {
            await _applicationDbContext.PaymentRecords.AddAsync(paymentRecord);
            await _applicationDbContext.SaveChangesAsync();
        }

        public IEnumerable<PaymentRecord> GetAllPaymentRecords() => _applicationDbContext.PaymentRecords.OrderBy(p => p.EmployeeId);
        public IEnumerable<SelectListItem> GetAllTaxYears()
        {
            var allTaxYear = _applicationDbContext.TaxYears.Select(taxYears => new SelectListItem
            {
                Text = taxYears.YearofTax,
                Value = taxYears.Id.ToString()
            });
            return allTaxYear;
        }

        public PaymentRecord GetPaymentRecordById(int id) => _applicationDbContext.PaymentRecords.Where(pay => pay.Id == id).FirstOrDefault();

        public decimal NetPayment(decimal totalEarning, decimal totalDeduction) => totalEarning - totalDeduction;

        public decimal OvertimeEarnings(decimal overtimeRate, decimal overtimeHours) => overtimeRate * overtimeHours;

        public decimal OvertimeHours(decimal hoursWorked, decimal contractualHours)
        {
            if (hoursWorked <= contractualHours)
            {
                overtimeHours = 0.00m;
            }
            else if (hoursWorked > contractualHours )
            {
                overtimeHours = hoursWorked - contractualHours;
            }
            return overtimeHours;
        }

        public decimal OvertimeRate(decimal hourlyRate) => hourlyRate * 1.5m;

        public decimal TotalDeduction(decimal tax, decimal nic, decimal studentLoanRepayment, decimal unionFees) => tax + nic + studentLoanRepayment + unionFees;

        public decimal TotalEarnings(decimal overtimeEarnings, decimal contractualEarings) => overtimeEarnings + contractualEarings;

        public TaxYear GetTaxYearById(int id) => _applicationDbContext.TaxYears.Where(year => year.Id == id).FirstOrDefault();
    }
}
