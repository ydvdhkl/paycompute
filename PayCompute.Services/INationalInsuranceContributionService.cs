﻿
namespace PayCompute.Services
{
    public interface INationalInsuranceContributionService
    {
        decimal INContribution(decimal totalAmount);
    }
}
